/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

var bcrypt = require('bcrypt-nodejs');

module.exports = {
      datastores: 'userMongodbServer',
      schema: true,

  attributes: {
    email: {
      type: 'string',
      isEmail: true,
      required: true,
      unique: true
    },
    username: {
      type: 'string',
      required: true,
      unique: true
    },
    phone: {
      type: 'string',
      unique: true,
      required: true
    },
    password: {
      type: 'string',
      required: true
    },
    hobbies:{
      
    }

  },

  beforeCreate: function(user, cb){
    bcrypt.genSalt(10,function(err,salt){
        bcrypt.hash(user.password, salt, null, function (err, harsh){
            if(err){
                console.log(err);
            }else{
                user.password = hash
            }
            cb();
        })
    })
  }

};

